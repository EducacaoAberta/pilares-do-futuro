jQuery(document).ready(function ($) {
    $(document).on('click', 'a.remove-image', function () {
        if (confirm('Atenção! Deseja realmente remover esse item?')) {
            $(this).parent('div.image-area').remove();
        }
    });
    function addTinyMce()
    {
         tinymce.init({
                selector: 'textarea.tiny-mce',
                menubar: '',
                plugins: 'code link lists',
                toolbar: 'undo redo | bold italic underline strikethrough | bullist | removeformat | link | code',
                relative_urls : false,
                remove_script_host : false,
                document_base_url : '<?php echo site_url(); ?>',
                setup: function (editor) {
                    editor.on('change', function (e) {
                        editor.save();
                    });
                }
            });
    }
    addTinyMce();
});