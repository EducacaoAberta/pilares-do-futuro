README.md

# Pilares do Futuro
A [Pilares do Futuro](https://pilaresdofuturo.org.br/) é uma plataforma colaborativa, criada para inserção de práticas educativas sobre cidadania digital. A concepção e o desenvolvimento foram feitos pelo [Instituto Educadigital](https://educadigital.org.br/) e a [Digimag](https://digimag.com.br). Coerente com nossos princípios de abertura e compartilhamento, disponibilizamos aqui os códigos para que qualquer pessoa/instituição interessada possa desenvolver plataforma semelhante com base nas taxonomias que foram criadas, a partir do plugin para [Wordpress](https://wordpress.org/) que foi desenvolvido.

## Requisitos
* PHP > 7.0
* Wordpress > 5.
