<?php
/**
 * Plugin Name: Pilares do Futuro
 * Description:
 * Author: André Keher
 * Version: 1.0
 * Author URI: https://github.com/andrekeher/
 */

class PilaresDoFuturo
{
    private static $instance;

    private function __construct()
    {
        require_once(dirname(__FILE__) . '/vendor/autoload.php');

        PilaresDoFuturo\Custom\User::getInstance();
        PilaresDoFuturo\Custom\Revision::getInstance();
        PilaresDoFuturo\Custom\Vote::getInstance();

        PilaresDoFuturo\Tool\ImgBB::getInstance();
        PilaresDoFuturo\Tool\Url::getInstance();

        PilaresDoFuturo\Meta\Download::getInstance();
        $highlight = PilaresDoFuturo\Meta\Highlight::getInstance();
        $highlight->postType = 'page';
        $highlight->init();

        PilaresDoFuturo\Meta\Notification::getInstance();

        PilaresDoFuturo\Taxonomy\AreaConhecimento::getInstance();
        PilaresDoFuturo\Taxonomy\CompetenciaGeral::getInstance();
        PilaresDoFuturo\Taxonomy\CompetenciaSocioemocional::getInstance();
        PilaresDoFuturo\Taxonomy\PublicoAlvo::getInstance();
        PilaresDoFuturo\Taxonomy\Tema::getInstance();

        PilaresDoFuturo\PostType\Pratica::getInstance();
        PilaresDoFuturo\PostType\Material::getInstance();
        PilaresDoFuturo\PostType\Apoiador::getInstance();

        PilaresDoFuturo\Custom\Author::getInstance();
        PilaresDoFuturo\Custom\Contact::getInstance();

        load_plugin_textdomain('pilares-do-futuro', false, dirname(plugin_basename(__FILE__)) . '/languages');

        register_activation_hook(__FILE__, function () {
            $curatorCaps = [];
            $editorCaps = get_role('editor')->capabilities;
            foreach ($editorCaps as $cap => $val) {
                if (strpos($cap, 'post') !== false || $cap === 'read') {
                    $cap = str_replace('post', 'pratica', $cap);
                    $curatorCaps[$cap] = $val;
                }
            }
            unset($editorCaps);
            add_role('curator', __('Curator', 'pilares-do-futuro'), $curatorCaps);

            add_role('practice_author', __('Practice author', 'pilares-do-futuro'), [
                'read' => true,
                'read_pratica' => true,
                'create_praticas' => true,
                'edit_pratica' => true,
                'edit_praticas' => true,
            ]);

            $this->updateWpRoles('activation');
        });

        register_deactivation_hook(__FILE__, function () {
            remove_role('curator');
            remove_role('practice_author');

            $this->updateWpRoles('deactivation');
        });

        add_action('admin_head', function () {
            wp_enqueue_style('_tinymce_css', includes_url('css/editor.min.css'));
            wp_enqueue_style('_admin', plugin_dir_url(__FILE__) . 'css/admin.css');
        });
        add_action('admin_footer', function () {
            wp_enqueue_script('_tinymce_js', includes_url('js/tinymce/tinymce.min.js'));
            wp_enqueue_script('_tinymce_js_code', plugin_dir_url(__FILE__) . 'js/tinymce/plugins/code/plugin.min.js');
            wp_enqueue_script('_tinymce_js_code', plugin_dir_url(__FILE__) . 'js/tinymce/plugins/lists/plugin.min.js');
            wp_enqueue_script('_admin', plugin_dir_url(__FILE__) . 'js/admin.js');
        });

        add_action('admin_menu', function () {
            remove_menu_page('edit.php');
        });

        add_filter('sanitize_file_name', 'remove_accents');

        add_post_type_support('page', 'excerpt');

        date_default_timezone_set('America/Sao_Paulo');
    }

    public function getWpDefaultRoles()
    {
        return [
            'administrator',
            'editor',
            'author',
            'contributor',
            'subscriber',
        ];
    }

    public function updateWpRoles($operation = 'activation')
    {
        $wpRoles = $this->getWpDefaultRoles();
        foreach ($wpRoles as $roleSlug) {
            $role = get_role($roleSlug);
            $roleCaps = $role->capabilities;
            foreach ($roleCaps as $cap => $value) {
                if ($operation === 'activation' && strpos($cap, 'post') !== false) {
                    $role->add_cap(str_replace('post', 'pratica', $cap));
                }
                if ($operation === 'deactivation' && strpos($cap, 'pratica') !== false) {
                    $role->remove_cap($cap);
                }
            }
        }
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
PilaresDoFuturo::getInstance();
