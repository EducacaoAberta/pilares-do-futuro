<?php
namespace AndreKeher\SimpleCaptcha;

class Captcha
{
    private $sessionKey = 'captchaSecret';
    
    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function setSessionKey($key)
    {
        $this->sessionKey = $key;
    }

    public function getImage($height, $width, $fontSize, $lenght, $alt = '', $title = '')
    {
        $secret = substr(str_shuffle('@#&23456789AaBbCcDdEeFfGgHhIiJjKkLlMmNnPpQqRrSsTtUuVvWwXxYyZz'), 0, ($lenght));
        $_SESSION[$this->sessionKey] = $secret;

        $image = imagecreate($height, $width);
        $grey = imagecolorallocate($image, 192, 192, 192);
        $white = imagecolorallocate($image, 255, 255, 255);
        $font = dirname(dirname(__FILE__)) . '/font/arial.ttf';

        for ($i = 1; $i <= $lenght; $i++) {
            imagettftext(
                $image,
                $fontSize,
                rand(-25, 25),
                ($fontSize * $i),
                ($fontSize + 10),
                $white,
                $font,
                substr($secret, ($i - 1), 1)
            );
        }

        ob_start();
        imagepng($image);
        imagedestroy($image);
        $image = ob_get_clean();

        return '<img src="data:image/png;base64,' . base64_encode($image) . '" alt="' . $alt . '" title="' . $title . '"/>';
    }

    public function isValid($value)
    {
        $value = strip_tags($value);
        return $_SESSION[$this->sessionKey] === $value;
    }
}
