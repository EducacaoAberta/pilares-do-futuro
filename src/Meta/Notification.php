<?php

namespace PilaresDoFuturo\Meta;

class Notification
{
    private static $instance;

    private function __construct()
    {
        add_action('save_post', static function () {
            $post = $GLOBALS['post'] ?? '';
            if (! is_object($post)) {
                return false;
            }
            if (get_post_type($post) !== 'pratica') {
                return false;
            }
            $data = get_post_meta($post->ID, '_notification', true);
            if (! empty($data)) {
                return false;
            }

            $headers = [];
            $curators = get_users(['role__in' => ['curator']]);
            if (! empty($curators)) {
                foreach ($curators as $curator) {
                    $headers[] = 'Bcc: ' . $curator->user_email;
                }
            }

            $adminEmail = get_option('admin_email');

            $author = get_user_by('ID', $post->post_author);
            $link = admin_url(sprintf('/post.php?post=%d&action=edit', $post->ID));

            $message = 'Título: ' . $post->post_title . '<br/>';
            $message .= 'URL: <a href="' . $link . '">' . $link . '</a><br/>';
            $message .= 'Autor: ' . $author->display_name . ' &lt;' . $author->user_email . '&gt;';

            $headers[] = 'Content-type: text/html;';
            wp_mail($adminEmail, 'Nova prática enviada', $message, $headers);

            update_post_meta($post->ID, '_notification', date('Y-m-d H:i:s'));
        });
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
