<?php

namespace PilaresDoFuturo\Meta;

use AndreKeher\WPDP\Metabox;

class Highlight
{
    private static $instance;
    public $postType;

    private function __construct()
    {
    }

    public function init()
    {
        if (empty($this->postType)) {
            return false;
        }
        $meta = new Metabox('_highlight_meta', 'Destaque na home', $this->postType);
        $meta->setFormFunction(function () {
            $post = $GLOBALS['post'];
            $_highlight = get_post_meta($post->ID, '_highlight', true);
            ?>
            <input type="checkbox" value="1" id="_highlight" name="_highlight" <?php echo !empty($_highlight) ? 'checked="checked"' : ''; ?>/>
            <label for="_highlight">Exibir como destaque na abertura da home?</label>
            <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('#_highlight').change(function(){
                    if ($(this).is(':checked')) {
                        alert('Atenção! Para a correta exibição na home, por favor, não se esqueça de configurar a imagem destacada da página. =)');
                    }
                });
            });
            </script>
            <?php
        });
        $meta->setSaveFunction(function () {
            $post = $GLOBALS['post'] ?? '';
            if (! is_object($post)) {
                return false;
            }
            extract($_POST);
            delete_post_meta($post->ID, '_highlight', true);
            if (isset($_highlight) && !empty($_highlight)) {
                update_post_meta($post->ID, '_highlight', 1);
            }
        });
        $meta->init();
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
