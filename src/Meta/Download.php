<?php

namespace PilaresDoFuturo\Meta;

use AndreKeher\WPDP\Columns;

class Download
{
    private static $instance;
    private $url;

    private function __construct()
    {
        add_action('template_redirect', function () {
            $wp = $GLOBALS['wp'];
            $this->url = home_url($wp->request);
            $this->registerForPratica();
            $this->registerForMaterial();
        });

        $column = new Columns(['pratica', 'material']);
        $label = (isset($_GET['post_type']) && $_GET['post_type'] === 'pratica') ? 'PDF<br/>Downloads' : 'Acessos';
        $column->setColumns(['_downloads' => $label]);
        $column->setDataFunction(function ($column) {
            if ($column !== '_downloads') {
                return false;
            }
            $post = $GLOBALS['post'] ?? '';
            if (! is_object($post)) {
                return false;
            }
            echo $this->getDownloads($post->ID);
        });
        $column->init();
    }

    private function getDownloads($postId)
    {
        return (int) get_post_meta($postId, '_downloads', true);
    }

    private function saveDownloads($postId, $downloads)
    {
        return update_post_meta($postId, '_downloads', $downloads);
    }

    private function registerForPratica()
    {
        $matches = [];
        if (preg_match('/^(pratica-)([0-9]+)-([a-z0-9-]+)$/', basename($this->url), $matches)) {
            $post = $this->register($matches[3], 'pratica');

            $file = WP_CONTENT_DIR . sprintf('/uploads/pdf/%s.pdf', basename($this->url));
            if (file_exists($file)) {
                wp_redirect(str_replace(WP_CONTENT_DIR, WP_CONTENT_URL, $file));
            }
            die;
        }
    }

    private function registerForMaterial()
    {
        $matches = [];
        if (preg_match('/(materiais)\/([a-z0-9\-]+)$/', $this->url, $matches)) {
            $post = $this->register($matches[2], 'material');

            $url = get_post_meta($post->ID, '_resource', true);
            if (@get_headers($url)[0]) {
                wp_redirect($url);
            }
            die;
        }
    }

    private function register($name, $postType)
    {
        $post = get_page_by_path($name, OBJECT, $postType);
        if (! is_object($post)) {
            wp_redirect(get_post_type_archive_link($postType));
            die;
        }
        $downloads = $this->getDownloads($post->ID);
        $this->saveDownloads($post->ID, ++$downloads);

        return $post;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
