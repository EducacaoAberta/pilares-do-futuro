<?php

namespace PilaresDoFuturo\Meta;

use \AndreKeher\WPDP\Metabox;

class Resource
{
    private $postType;
    private $id;
    private $title;
    private $enqueueMedia;

    public function __construct($postType, $id, $title, $enqueueMedia = true)
    {
        $this->postType = $postType;
        $this->id = $id;
        $this->title = $title;
        $this->enqueueMedia = $enqueueMedia;
    }

    public function init()
    {
        $meta = new Metabox($this->id, $this->title, $this->postType);
        $meta->setFormFunction([$this, 'showForm']);
        $meta->setSaveFunction([$this, 'saveForm']);
        $meta->init();
    }

    public function showForm()
    {
        $post = $GLOBALS['post'] ?? '';
        if (! is_object($post)) {
            return false;
        }
        if ($this->enqueueMedia) {
            wp_enqueue_media($post->ID);
        }
        $meta = get_post_meta($post->ID, $this->id, true);
        ?>
        <input type="url" name="<?php echo $this->id; ?>" value="<?php echo $meta; ?>" required="required" placeholder="Informe o link" class="widefat"/>
        <script type="text/javascript">
        jQuery(document).ready(function($){
            $('[name="_<?php echo $this->id; ?>_load_link"]').on('click', function(e) {
                e.preventDefault();
                var file_frame = wp.media({
                    title: '<?php echo $this->title; ?>',
                    button: {
                        text: 'Utilizar item selecionado',
                    },
                    multiple: false
                });
                file_frame.on('select', function() {
                    var attachment = file_frame.state().get('selection').first().toJSON();
                    $('[name="<?php echo $this->id; ?>"]').val(attachment.url);
                });
                file_frame.open();
            });
        });
        </script>
        <?php
        if ($this->enqueueMedia) {
            submit_button('Carregar link da biblioteca de mídia', 'primary', '_' . $this->id . '_load_link');
        }
    }

    public function saveForm()
    {
        if (! is_user_logged_in()) {
            return false;
        }
        if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX)) {
            return false;
        }
        if (! isset($_POST[$this->id]) || ! filter_var($_POST[$this->id], FILTER_VALIDATE_URL)) {
            return false;
        }

        $post = $GLOBALS['post'] ?? '';
        if (! is_object($post)) {
            return false;
        }
        update_post_meta($post->ID, $this->id, $_POST[$this->id]);
    }
}
