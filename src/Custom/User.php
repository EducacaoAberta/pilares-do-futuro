<?php

namespace PilaresDoFuturo\Custom;

use AndreKeher\SimpleCaptcha\Captcha;
use AndreKeher\WPDP\SubMenuPage;

class User
{
    private static $instance;
    private static $estados = [
        'AC',
        'AL',
        'AM',
        'AP',
        'BA',
        'CE',
        'DF',
        'ES',
        'GO',
        'MA',
        'MG',
        'MS',
        'MT',
        'PA',
        'PB',
        'PE',
        'PI',
        'PR',
        'RJ',
        'RN',
        'RO',
        'RR',
        'RS',
        'SC',
        'SE',
        'SP',
        'TO',
    ];
    private $captcha;

    private function __construct()
    {
        self::$estados[] = __('International', 'pilares-do-futuro');

        $this->captcha = new Captcha();

        add_action('register_form', [$this, 'customizeRegisterForm']);
        add_filter('registration_errors', [$this, 'customizeRegistrationErrors'], 10, 3);
        add_action('user_register', [$this, 'customizeUserRegister']);

        add_action('edit_user_profile', [$this, 'customizeUserNewForm']);
        add_action('profile_update', array($this, 'customizeUserRegister'));
        add_action('show_user_profile', [$this, 'customizeUserNewForm']);

        add_action('login_enqueue_scripts', [$this, 'customizeLoginEnqueueScripts']);

        add_shortcode('curadores', [$this, 'showCurators']);

        $subPage = new SubMenuPage('users.php', 'Exportar curadores e autores de práticas', 'Exportar', '_exportar_usuarios', 'manage_options', 25);
        $subPage->setFormFunction([$this, 'showExportForm']);
        $subPage->setSaveFunction([$this, 'processExportForm']);
        $subPage->init();

        add_action('init', [$this, 'updateYoastSEOBehavior']);

        add_action('admin_bar_menu', function ($wpAdminBar) {
            $wpAdminBar->remove_node('wp-logo');
        }, 100);
    }

    public static function getCustomizeDataFromPost()
    {
        return ! empty($_POST['_estado']) ? sanitize_text_field($_POST['_estado']) : '';
    }

    public static function getCustomizeDataFromDb($userId)
    {
        return get_user_meta($userId, '_estado', true);
    }

    public static function showCustomizeFieldState($estado)
    {
        ?>
        <select id="_estado" name="_estado" class="input" <?php echo (is_admin()) ? '' : 'style="appearance:menulist; -webkit-appearance:menulist; -moz-appearance:menulist; -ms-appearance:menulist;"' ?>>
            <option value="">--</option>
            <?php
            foreach (self::$estados as $option) {
                echo '<option value="', $option, '" ', ($estado === $option) ? ' selected="selected"' : '', '>', __($option, 'pilares-do-futuro'), '</option>';
            }
            ?>
        </select>
        <?php
    }

    public function customizeRegisterForm()
    {
        $estado = self::getCustomizeDataFromPost();
        ?>
        <p>
            <label for="_estado"><?php echo __('State', 'pilares-do-futuro'); ?><br/>
                <?php self::showCustomizeFieldState($estado); ?>
            </label>
        </p>
        <p>
            <label for="_captcha"><?php echo __('Enter the characters in the image', 'pilares-do-futuro'); ?><br/>
                <input type="text" name="_captcha"/>
                <?php
                echo $this->captcha->getImage(215, 50, 30, 6);
                ?>
            </label>
        </p>
        <?php
    }

    public function customizeRegistrationErrors($errors, $sanitizedUserLogin, $email)
    {
        if (empty($_POST['_estado'])) {
            $errors->add('_estado', __('<strong>ERROR</strong>: Enter your state.', 'pilares-do-futuro'));
        }
        if (! $this->captcha->isValid($_POST['_captcha'])) {
            $errors->add('_captcha', __('<strong>ERROR</strong>: Enter the characters in the image.', 'pilares-do-futuro'));
        }
        return $errors;
    }

    public function customizeUserRegister($userId)
    {
        if (! empty($_POST['_estado'])) {
            update_user_meta($userId, '_estado', sanitize_text_field($_POST['_estado']));
        }
        if (isset($_FILES['_photo']) && ! empty($_FILES['_photo']['tmp_name'])) {
            $fileOpen = finfo_open(FILEINFO_MIME_TYPE);
            $fileMime = finfo_file($fileOpen, $_FILES['_photo']['tmp_name']);
            if (strpos($fileMime, 'image/') !== false) {
                $uploadDir = wp_get_upload_dir()['basedir'] . '/photo/';
                $file = md5($_POST['user_id']) . '.' . pathinfo($_FILES['_photo']['name'], PATHINFO_EXTENSION);

                if (move_uploaded_file($_FILES['_photo']['tmp_name'], $uploadDir . $file)) {
                    update_user_meta($_POST['user_id'], '_photo', $file);
                }
            }
        }
    }

    public function customizeUserNewForm($user)
    {
        $estado = self::getCustomizeDataFromDb($user->ID);
        ?>
        <table class="form-table">
            <tr>
                <th><label for="_estado"><?php echo __('State', 'pilares-do-futuro'); ?></label></th>
                <td>
                    <?php self::showCustomizeFieldState($estado); ?>
                </td>
            </tr>
            <?php
            if (current_user_can('edit_others_praticas')) {
                ?>
                <tr>
                    <th><label for="_photo"><?php echo __('Profile Picture'); ?></label></th>
                    <td>
                        <img alt="" title="" src="<?php echo wp_get_upload_dir()['baseurl'] . '/photo/' . get_user_meta($user->ID, '_photo', true); ?>" class="avatar avatar-96 photo" width="96" height="96" />
                        <br />
                        <input type="file" name="_photo" id="_photo" accept="image/*"/>
                    </td>
                </tr>
                <script type="text/javascript">
                jQuery(document).ready(function($){
                    $('tr.user-profile-picture').remove();
                    $('#your-profile').attr('enctype', 'multipart/form-data');
                });
                </script>
                <?php
            }
            ?>
        </table>
        <?php
    }

    public function showCurators()
    {
        ob_start();
        ?>
        <div class="curators list center">
            <?php
            $curators = get_users(['role__in' => ['curator']]);
            foreach ($curators as $curator) {
                ?>
                <div class="item-curador">
                    <div class="bg-curador"></div>
                    <div class="foto-curador" style="background: url('<?php echo wp_get_upload_dir()['baseurl'] . '/photo/' . get_user_meta($curator->ID, '_photo', true); ?>');"></div>
                    <div class="info-curador">
                        <h3 class="nome"><?php echo $curator->display_name; ?></h3>
                        <div><?php echo wpautop($curator->description); ?></div>
                        <?php
                        if (!empty($curator->user_url)) {
                            ?>
                            <a class="btn-curador" href="<?php echo $curator->user_url; ?>">Saiba mais</a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
        return ob_get_clean();
    }

    public function customizeLoginEnqueueScripts()
    {
        ?>
        <style type="text/css">
        body.login div#login h1 a {
            width: 100%;
            background: url("<?php echo get_template_directory_uri(); ?>/images/pilares-do-futuro.png") no-repeat scroll center;
            background-size: contain;
            padding: 8px 0;
        }
        </style>
        <?php
    }

    public function showExportForm()
    {
        ?>
        <h1>Exportar curadores e autores de práticas</h1>
        <form method="post" action="<?php echo admin_url('users.php?page=_exportar_usuarios'); ?>">
            <input type="hidden" name="action" value="_exportar_usuarios"/>
            <?php
            wp_nonce_field();
            submit_button('Exportar');
            ?>
        </form>
        <?php
    }

    public function processExportForm()
    {
        if (! isset($_POST['action']) || $_POST['action'] !== '_exportar_usuarios') {
            return false;
        }
        if (! isset($_POST['_wpnonce']) || ! wp_verify_nonce($_POST['_wpnonce'])) {
            return false;
        }

        $users = get_users([
            'role__in' => [
                'curator',
                'practice_author'
            ],
            'orderby' => 'user_registered',
        ]);

        header('Content-encoding: UTF-8');
        header('Content-type: application/csv; charset=UTF-8');
        header('Content-disposition: attachment; filename=curadores_autores_pratica_' . date('Ymd_His') . '.csv');

        $data = 'Nome;E-mail;Registro;' . PHP_EOL;
        foreach ($users as $user) {
            $data .= sprintf('%s;%s;%s;%s', $user->display_name, $user->user_email, $user->user_registered, PHP_EOL);
        }
        echo mb_convert_encoding($data, 'UTF-16LE', 'UTF-8');
        die;
    }

    public function updateYoastSEOBehavior()
    {
        $user = wp_get_current_user();
        if (in_array('practice_author', (array) $user->roles)) {
            add_filter('manage_edit-pratica_columns', function ($columns) {
                unset($columns['wpseo-score-readability']);
                unset($columns['wpseo-score']);
                unset($columns['wpseo-title']);
                unset($columns['wpseo-metadesc']);
                unset($columns['wpseo-focuskw']);
                return $columns;
            });

            add_action('add_meta_boxes', function () {
                remove_meta_box('wpseo_meta', 'pratica', 'normal');
            }, 100000);
        }
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
