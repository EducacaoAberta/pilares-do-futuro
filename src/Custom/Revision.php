<?php

namespace PilaresDoFuturo\Custom;

use AndreKeher\WPDP\Ajax;
use AndreKeher\WPDP\Metabox;

class Revision
{
    private static $instance;

    private function __construct()
    {
        add_action('admin_init', function () {
            if (current_user_can('edit_others_praticas') || current_user_can('edit_praticas')) {
                $metabox = new Metabox('_revisions', 'Revisão', 'pratica');
                $metabox->setFormFunction([$this, 'createForm']);
                $metabox->setSaveFunction(static function () {
                    return true;
                });
                $metabox->init();
            }
        });

        $ajaxSave = new Ajax('_save_revision');
        $ajaxSave->setFunction([$this, 'saveRevision']);
        $ajaxSave->init();

        $ajaxUpdate = new Ajax('_update_revision');
        $ajaxUpdate->setFunction([$this, 'updateRevision']);
        $ajaxUpdate->init();
    }

    public function getRevisions($postId)
    {
        $revisions = [];
        $data = get_post_meta($postId, '_revision');
        foreach ($data as $d) {
            $revisions[] = json_decode($d, true);
        }
        return $revisions;
    }

    public function createForm()
    {
        $post = $GLOBALS['post'];
        ?>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
        <h3 class="revisions" style="display:none;">Por favor, realize a(s) seguinte(s) melhoria(s) na prática:</h3>
        <div class="revisions">
            <?php
            $revisions = $this->getRevisions($post->ID);
            echo $this->getFormatRevisions($revisions);
            ?>
        </div>
        <script>
        function actionsForRevisions() {
            let $ = jQuery.noConflict(),
                isCurator = '<?php echo (bool) current_user_can('edit_others_praticas'); ?>';

            $('div.revision').each(function(){
                $(this).find('.material-icons').remove();
                $(this).prepend('<span class="material-icons"></span>');
                let mi = $(this).find('.material-icons');
                if ($(this).data('token') != '') {
                    if (! isCurator) {
                        $(mi).html('<a href="#" title="Arquivar">archive</a>');
                    }
                } else {
                    $(mi).html('<span title="OK">check</span>');
                }
            });
        }
        jQuery(document).ready(function($){
            if ($('div.revisions').find('*').length > 0) {
                $('h3.revisions, h4.revisions').fadeIn();
            }
            actionsForRevisions();
        });
        </script>
        <?php
        if (current_user_can('edit_others_praticas')) {
            ?>
            <div>
                <input type="hidden" name="_revision[post_id]" value="<?php echo $post->ID; ?>"/>
                <h3>Enviar nova solicitação de melhoria na prática</h3>
                <textarea class="widefat" name="_revision[request]"></textarea>
                <br />
                <br />
                <?php submit_button('Enviar', 'primary', 'send_revision', false); ?>
                <img src="<?php echo admin_url('images/loading.gif'); ?>" style="display: none;" alt="Processando..." title="Processando..."/>
            </div>
            <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('[name="send_revision"]').click(function(e){
                    e.preventDefault();
                    let me = $(this);
                    if ($('[name="_revision[request]"]').val().trim() !== '') {
                        $(me).hide().next().show();
                        $.ajax({
                            type: 'post',
                            url: ajaxurl + '?action=_save_revision',
                            data: $('[name^="_revision["]').serialize(),
                            dataType: 'html',
                            success: function(d) {
                                $('[name="_revision[request]"]').val('');
                                $('div.revisions').find('*').remove();
                                $('h3.revision').fadeIn();
                                $('div.revisions').html(d);
                                $(me).show().next().hide();
                                actionsForRevisions();
                            },
                            error: function(xhr, status, error) {
                                console.log(xhr);
                                let errMsg = '';
                                if (xhr.responseText === '') {
                                    errMsg = 'Erro ao processar solicitação.\n\nPor favor, tente novamente mais tarde.\n\nDetalhes:\n' + error;
                                } else {
                                    errMsg = 'Atenção!\n\n' + xhr.responseText;
                                }
                                alert(errMsg);
                            },
                            complete: function() {
                            }
                        });
                    }
                });
            });
            </script>
            <?php
        } else {
            ?>
            <h4 class="revisions" style="display: none;">Clique em <span class="material-icons">archive</span> para arquivar e notificar o curador de uma melhoria já realizada na prática.</h4>
            <script type="text/javascript">
            jQuery(document).ready(function($){
                $(document).on('click', '.revision a',function(e) {
                     e.preventDefault();
                     if (! confirm('Deseja realmente arquivar?')) {
                        return false;
                     }
                     let me = $(this),
                        token = $(me).closest('div.revision').data('token');
                     $.ajax({
                        type: 'post',
                        url: ajaxurl + '?action=_update_revision',
                        data: {_token: token},
                        dataType: 'text',
                        success: function(d) {
                            if (d == 1) {
                                $(me).closest('div.revision').data('token', '');
                                $(me).remove();
                                actionsForRevisions();
                            }
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr);
                            let errMsg = '';
                            if (xhr.responseText === '') {
                                errMsg = 'Erro ao processar solicitação.\n\nPor favor, tente novamente mais tarde.\n\nDetalhes:\n' + error;
                            } else {
                                errMsg = 'Atenção!\n\n' + xhr.responseText;
                            }
                            alert(errMsg);
                        },
                        complete: function() {
                        }
                    });
                });
            });
            </script>
            <?php
        }
    }

    public function saveRevision()
    {
        if (! is_user_logged_in()) {
            return false;
        }
        if (! current_user_can('edit_others_praticas')) {
            return false;
        }
        if (! isset($_POST['_revision'])) {
            return false;
        }
        extract($_POST['_revision']);
        if (! isset($post_id) || (int) $post_id <= 0 || is_null(get_post($post_id))) {
            return false;
        }
        if (! isset($request) || ! is_string($request)) {
            return false;
        }

        unset($_POST['_revision']['post_id']);
        $_POST['_revision']['user_id'] = get_current_user_id();
        $_POST['_revision']['date'] = time();
        $_POST['_revision']['token'] = md5(uniqid(rand(), true));
        add_post_meta($post_id, '_revision', json_encode($_POST['_revision'], JSON_UNESCAPED_UNICODE));

        $revisions = $this->getRevisions($post_id);
        echo $this->getFormatRevisions($revisions);

        $post = get_post($post_id);

        $msg = 'Prática: ' . $post->post_title . '<br/>';
        $url = admin_url(sprintf('post.php?post=%d&action=edit', $post_id));
        $msg .= 'URL: <a href="' . $url . '">' . $url . '</a><br/>';
        $msg .= '<br/>' . $this->getFormatRevision($_POST['_revision']);

        wp_mail(get_user_by('id', $post->post_author)->user_email, 'Solicitação de melhoria na prática', $msg, 'Content-type: text/html;');

        wp_die();
    }

    public function updateRevision()
    {
        if (! is_user_logged_in()) {
            return false;
        }
        if (current_user_can('edit_others_praticas')) {
            return false;
        }
        if (! isset($_POST['_token'])) {
            return false;
        }

        $wpdb = $GLOBALS['wpdb'] ?? '';
        if (! is_object($wpdb)) {
            return false;
        }

        extract($_POST);
        $_token = strip_tags($_token);

        $result = 0;
        $data = $wpdb->get_row(sprintf('SELECT * FROM %s WHERE meta_value LIKE "%%%s%%";', $wpdb->prefix . 'postmeta', $_token), ARRAY_A);
        if (is_array($data) && isset($data['meta_value'])) {
            $data['meta_value'] = str_replace($_token, '', $data['meta_value']);
            $result = (int) $wpdb->query(sprintf('UPDATE %s SET meta_value = "%s" WHERE meta_id = %d;', $wpdb->prefix . 'postmeta', esc_sql($data['meta_value']), $data['meta_id']));
        }
        if (! empty($result)) {
            $revision = json_decode($data['meta_value'], true);

            $userId = $revision['user_id'];

            $msg = 'Prática: ' . get_the_title($data['post_id']) . '<br/>';
            $url = admin_url(sprintf('post.php?post=%d&action=edit', $data['post_id']));
            $msg .= 'URL: <a href="' . $url . '">' . $url . '</a><br/>';
            $msg .= '<br/>' . $this->getFormatRevision($revision);

            wp_mail(get_user_by('id', $userId)->user_email, 'Melhoria realizada na prática', $msg, 'Content-type: text/html;');
        }

        echo $result;
        wp_die();
    }

    private function getFormatRevision($revision)
    {
        return '<div class="revision request" data-token="' . $revision['token'] . '">' . $revision['request'] . '<br/><i>por ' . get_user_by('id', $revision['user_id'])->display_name . ' em ' . date('d/m/Y H:i:s', $revision['date']) . '</i><br/><br/><hr/><br/></div>';
    }

    public function getFormatRevisions($revisions)
    {
        $html = '';
        if (! empty($revisions) && is_array($revisions)) {
            foreach ($revisions as $revision) {
                $html .= $this->getFormatRevision($revision);
            }
        }
        return $html;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
