<?php

namespace PilaresDoFuturo\Custom;

use AndreKeher\WPDP\Ajax;
use AndreKeher\WPDP\Metabox;

class Vote
{
    private static $instance;

    private function __construct()
    {
        add_action('admin_init', function () {
            if (current_user_can('edit_others_praticas')) {
                $metabox = new Metabox('_vote', 'Votos para publicação', 'pratica');
                $metabox->setFormFunction([$this, 'createForm']);
                $metabox->setSaveFunction(static function () {
                    return true;
                });
                $metabox->init();
            }
        });

        $ajax = new Ajax('_save_vote');
        $ajax->setFunction([$this, 'saveVote']);
        $ajax->init();

        add_action('post_updated', [$this, 'publicationControl']);
        add_action('admin_notices', [$this, 'showNoticeAboutVotes']);
    }

    private function getVotes($postId)
    {
        $votes = [];
        $data = get_post_meta($postId, '_vote');
        foreach ($data as $vote) {
            $votes[] = json_decode($vote, true);
        }
        return $votes;
    }

    private function getFormatVotes($votes)
    {
        $html = '';
        if (empty($votes) && ! is_array($votes)) {
            return $html;
        }
        foreach ($votes as $vote) {
            $html .= '<div class="vote"><i>por ' . get_user_by('id', $vote['user_id'])->display_name . ' em ' . date('d/m/Y H:i:s', $vote['date']) . '</i><br/><br/><hr/><br/></div>';
        }
        return $html;
    }

    public function createForm()
    {
        $post = $GLOBALS['post'];
        ?>
        <div class="votes">
            <?php
            $votes = $this->getVotes($post->ID);
            echo $this->getFormatVotes($votes);
            ?>
        </div>
        <?php
        if (current_user_can('edit_others_praticas')) {
            ?>
            <input type="hidden" name="_vote[post_id]" value="<?php echo $post->ID; ?>"/>
            <?php
            submit_button('Enviar voto', 'primary', 'send_vote');
            ?>
            <img src="<?php echo admin_url('images/loading.gif'); ?>" style="display: none;" alt="Processando..." title="Processando..."/>
            <script>
            jQuery(document).ready(function($) {
                $('[name="send_vote"]').click(function(e){
                    e.preventDefault();
                    let me = $(this);
                    $(me).hide().next().show();
                    $.ajax({
                        type: 'post',
                        url: ajaxurl + '?action=_save_vote',
                        data: $('[name^="_vote["]').serialize(),
                        dataType: 'html',
                        success: function(d) {
                            $('div.votes').find('*').remove();
                            $('div.votes').html(d);
                            $(me).show().next().hide();
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr);
                            let errMsg = '';
                            if (xhr.responseText === '') {
                                errMsg = 'Erro ao processar solicitação.\n\nPor favor, tente novamente mais tarde.\n\nDetalhes:\n' + error;
                            } else {
                                errMsg = 'Atenção!\n\n' + xhr.responseText;
                            }
                            alert(errMsg);
                        },
                        complete: function() {
                        }
                    });
                });
            });
            </script>
            <?php
        }
    }

    public function saveVote()
    {
        if (! current_user_can('edit_others_praticas')) {
            return false;
        }
        if (! isset($_POST['_vote'])) {
            return false;
        }
        extract($_POST['_vote']);
        if (! isset($post_id) || (int) $post_id <= 0 || is_null(get_post($post_id))) {
            return false;
        }

        $wpdb = $GLOBALS['wpdb'] ?? '';
        if (! is_object($wpdb)) {
            return false;
        }

        $userId = get_current_user_id();
        $voted = $wpdb->get_var(sprintf(
            "SELECT
                COUNT(*)
            FROM wp_postmeta
            WHERE meta_key = '_vote'
                AND post_id = %d
                AND meta_value LIKE '{\"user_id\":%d,%%'",
            $post_id,
            $userId
        ));
        if (! empty($voted)) {
            http_response_code(500);
            echo 'Você já votou na aprovação dessa prática.';
            die;
        }

        unset($_POST['_vote']['post_id']);
        $_POST['_vote']['user_id'] = $userId;
        $_POST['_vote']['date'] = time();
        add_post_meta($post_id, '_vote', json_encode($_POST['_vote']));

        $votes = $this->getVotes($post_id);
        echo $this->getFormatVotes($votes);

        die;
    }

    public function publicationControl()
    {
        if (! current_user_can('publish_praticas')) {
            return false;
        }
        $post = $GLOBALS['post'] ?? '';
        if (! is_object($post)) {
            return false;
        }
        if (! isset($post->post_type) || $post->post_type !== 'pratica') {
            return false;
        }
        if ($post->post_status !== 'pending') {
            return false;
        }

        $wpdb = $GLOBALS['wpdb'];
        $sql = sprintf(
            'SELECT
                COUNT(post_id)
            FROM %spostmeta
            WHERE meta_key = "_vote"
                AND post_ID = %d;',
            $wpdb->prefix,
            $post->ID
        );
        $data = $wpdb->get_var($sql);
        if ($data < 2) {
            $sql = sprintf(
                'UPDATE
                    %sposts
                SET post_status = "pending"
                WHERE ID = %d;',
                $wpdb->prefix,
                $post->ID
            );
            $wpdb->query($sql);

            wp_redirect(admin_url(sprintf('post.php?post=%d&action=edit&_votes=%d', $post->ID, $data)));
            die();
        }
    }

    public function showNoticeAboutVotes()
    {
        $post = $GLOBALS['post'] ?? '';
        if (! is_object($post)) {
            return false;
        }
        if ($post->post_type === 'pratica' && isset($_GET['_votes']) && $_GET['_votes'] < 2) {
            ?>
            <div class="notice notice-error is-dismissible">
                <p><?php _e(sprintf('São necessários 2 votos para para a publicação da prática.<br/>Votos para a publicação até o momento: %d', $_GET['_votes']), 'sample-text-domain'); ?></p>
            </div>
            <?php
        }
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
