<?php

namespace PilaresDoFuturo\Custom;

class Author
{
    private static $instance;

    private function __construct()
    {
        add_action('init', [$this, 'updateAuthorPermalinks']);
        add_filter('query_vars', [$this, 'addVar']);
        add_filter('generate_rewrite_rules', [$this, 'authorRewriteRules']);
        add_action('pre_get_posts', [$this, 'updateQuery']);
    }

    public function updateAuthorPermalinks()
    {
        $wp_rewrite = $GLOBALS['wp_rewrite'] ?? '';
        if (! is_object($wp_rewrite)) {
            return false;
        }
        $wp_rewrite->author_base = 'autor';
        $wp_rewrite->flush_rules();
    }
    
    public function addVar($vars)
    {
        $newVar = ['autor'];
        $vars = $newVar + $vars;
        return $vars;
    }
    
    public function authorRewriteRules($wp_rewrite)
    {
        $newRule = [];
        $newRule['autor/(\d*)$'] = 'index.php?author=$matches[1]';
        $wp_rewrite->rules = $newRule + $wp_rewrite->rules;
    }
    
    public function updateQuery($query)
    {
        if (! is_admin() && $query->is_main_query() && $query->is_author()) {
            $query->set('post_type', array('pratica'));
        }
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
