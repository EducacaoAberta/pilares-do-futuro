<?php

namespace PilaresDoFuturo\Custom;

use AndreKeher\SimpleCaptcha\Captcha;
use AndreKeher\WPDP\MenuPage;

class Contact
{
    private static $instance;
    private $fields = [
        'nome' => [
            'label' => 'Nome',
            'type' => 'text',
        ],
        'email' => [
            'label' => 'E-mail',
            'type' => 'text',
        ],
        'assunto' => [
            'label' => 'Assunto',
            'type' => 'text',
        ],
        'mensagem' => [
            'label' => 'Mensagem',
            'type' => 'textarea',
        ],
        'captcha' => [
            'label' => 'Digite os caracteres na imagem',
            'type' => 'text',
        ]
    ];
    private $captcha;

    public function __construct()
    {
        $this->captcha = new Captcha();

        add_shortcode('contato', [$this, 'generateForm']);

        add_action('template_redirect', function () {
            if ($this->validate()) {
                $this->sendForm();
            }
        });

        $page = new MenuPage('Contato', 'Contato', '_emails_contato', 'edit_others_posts');
        $page->setFormFunction(function () {
            $data = $this->getEmails();
            ?>
            <h1>Configure abaixo os e-mails que devem receber as interações do formulário de <a href="<?php echo site_url('contato'); ?>" target="_blank">contato</a>.</h1>
            <?php
            if (isset($_GET['status'])) {
                ?>
                <div class="notice notice-success is-dismissible">
                    <p>Salvo com sucesso!</p>
                </div>
                <?php
            }
            ?>
            <br />
            <form method="post" action="<?php echo admin_url('edit.php?post_type=pratica&page=_emails_contato'); ?>">
                <input type="hidden" name="redirect_to" value="<?php echo admin_url('edit.php?post_type=pratica&page=_emails_contato&status=ok'); ?>"/>
                <textarea name="_emails_contato" class="widefat" required="required" rows="7"><?php echo $data; ?></textarea>
                <b><i class="orange" style="color:orange;font-size:16px;">Atenção! Por favor, informe um endereço de e-mail por linha.</i></b>
                <?php submit_button('Salvar e-mails'); ?>
            </form>
            <?php
        });
        $page->setSaveFunction(function () {
            if (! current_user_can('edit_others_posts')) {
                return false;
            }
            if (! isset($_POST['_emails_contato']) || empty($_POST['_emails_contato'])) {
                return false;
            }
            $emails = trim(strip_tags($_POST['_emails_contato']));
            $emails = explode(PHP_EOL, $emails);
            if (empty($emails) || ! is_array($emails)) {
                return false;
            }
            foreach ($emails as $key => $email) {
                $emails[$key] = $email = trim($email);
                if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    unset($emails[$key]);
                }
            }
            update_option('_emails_contato', json_encode($emails));
            wp_redirect($_POST['redirect_to']);
            die;
        });
        $page->init();
    }

    public function generateForm()
    {
        ob_start();
        if (isset($_GET['status'])) {
            ?>
            <div class="notice-success" style="border: 1px solid green; border-radius: 3px; padding-left: 14px;">
                <p>Enviado com sucesso! Obrigado(a) =)</p>
            </div>
            <?php
        }
        ?>
        <form id="contact" method="post">
            <?php
            wp_nonce_field('contato');
            foreach ($this->fields as $name => $attrs) {
                ?>
                <div>
                    <label for="<?php echo $name; ?>"><?php echo $attrs['label']; ?>: <span class="required">*</span></label>
                    <?php
                    $value = $_POST[$name] ?? '';
                    if ($attrs['type'] === 'textarea') {
                        echo '<textarea class="widefat" name="', $name, '" required="required">', $value, '</textarea>';
                    } else {
                        echo '<input type="', $attrs['type'], '" class="widefat" name="', $name, '" value="', $value, '" required="required"/>';
                    }
                    if (isset($attrs['error'])) {
                        echo '<div class="error red">', $attrs['error'], '</div>';
                    }
                    ?>
                </div>
                <?php
            }
            ?>
            <div>
                <?php echo $this->captcha->getImage(215, 50, 30, 6); ?>
            </div>
            <input type="submit" value="Enviar" class="btn primary btn-primary"/>
        </form>
        <?php
        return ob_get_clean();
    }

    public function validate()
    {
        if (! isset($_POST['_wpnonce']) || ! wp_verify_nonce($_POST['_wpnonce'], 'contato')) {
            return false;
        }
        array_walk_recursive($_POST, function ($val, $key) {
            $_POST[$key] = trim(strip_tags($val));
        });
        $hasError = null;
        foreach (array_keys($this->fields) as $field) {
            if (! isset($_POST[$field]) || empty($_POST[$field])) {
                $this->fields[$field]['error'] = 'Por favor, informe corretamente esse campo.';
                $hasError = true;
            }
        }
        if (! $this->captcha->isValid($_POST['captcha'])) {
                $this->fields['captcha']['error'] = 'Atenção! Digite corretamente os caracteres na imagem.';
                $hasError = true;
        }
        if ($hasError) {
            return false;
        }

        return true;
    }

    public function sendForm()
    {
        if (! $_POST) {
            return false;
        }
        
        unset($this->fields['captcha']);

        $message = '';
        foreach ($this->fields as $name => $attrs) {
            $message .= sprintf('%s: %s', $attrs['label'], $_POST[$name]) . PHP_EOL;
        }

        $emails = $this->getEmails(true);
        if (empty($emails)) {
            $emails = get_option('admin_email');
        }

        $siteTitle = get_option('blogname');
        if (! wp_mail($emails, $siteTitle . ' | Contato', $message)) {
            error_log('Falha ao enviar e-mail: ' . $message);
            wp_die('Falhar ao enviar e-mail. Por favor, tente novamente mais tarde.');
        }

        global $wp;
        wp_redirect(add_query_arg('status', 'ok', home_url($wp->request)));
        die;
    }

    private function getEmails($forSend = false)
    {
        $data = json_decode(get_option('_emails_contato'), true);
        if (! is_array($data)) {
            return false;
        }
        if ($forSend) {
            return $data;
        }
        return implode(PHP_EOL, $data);
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
