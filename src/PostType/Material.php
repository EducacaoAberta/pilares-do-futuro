<?php
namespace PilaresDoFuturo\PostType;

use AndreKeher\WPDP\Metabox;
use AndreKeher\WPDP\Post;
use \PilaresDoFuturo\Meta\Resource;

class Material
{
    private static $instance;
    private $postType;
    
    private function __construct()
    {
        $postType = new Post('material', 'materiais', 'Material', 'Material', 'Materiais', '', false);
        $postType->setArgs('supports', ['title', 'thumbnail']);
        $this->postType = $postType->init();
        
        $resource = new Resource($this->postType, '_resource', 'Recurso', true);
        $resource->init();
        \PilaresDoFuturo\Taxonomy\Tipo::getInstance($this->postType);
    }
    
    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
