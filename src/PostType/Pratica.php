<?php
namespace PilaresDoFuturo\PostType;

use AndreKeher\WPDP\Metabox;
use AndreKeher\WPDP\Post;
use AndreKeher\WPDP\Columns;
use PilaresDoFuturo\Tool\Pdf;

class Pratica
{
    private static $instance;
    private $postType;
    private $fields = [
        'coautor' => [
            'label' => 'Coautor(a) ou coautores',
            'after' => 'Alguém mais elaborou ou desenvolveu essa prática com você? Coloque os nomes das pessoas.',
            'required' => false,
            'title' => 'Coautoria',
        ],
        'contexto' => [
            'label' => 'Qual contexto te motivou a elaborar a boa prática?',
            'after' => 'Conte como surgiu a ideia, qual o problema ou desafio que você identificou e, caso a prática já tenha sido realizada, descreva qual era a situação que precisava ser aprimorada.',
            'required' => true,
            'title' => 'Contexto',
        ],
        'objetivos' => [
            'label' => 'Quais os objetivos da prática?',
            'after' => 'De forma resumida, enumere os objetivos/metas para a implementação dessa prática. Por favor, informe um item por linha.',
            'required' => true,
            'title' => 'Objetivos',
        ],
        'recursos_educativos' => [
            'label' => 'Utiliza recursos educativos? Quais?',
            'after' => 'Para implementar essa prática, quais recursos são necessários? Materiais educativos impressos ou audiovisuais de apoio? Materiais de uso comum? Algo especial?',
            'required' => false,
            'title' => 'Recursos educativos',
        ],
        'metodologia' => [
            'label' => 'Descreva a metodologia',
            'after' => 'Procure descrever com detalhes como implementar essa prática, contemplando a organização da turma, os primeiros passos para ambientação e/ou apresentação da proposta, o desenvolvimento dos trabalhos e a conclusão. Caso você tenha utilizado elementos de cultura maker, robótica, atividade desplugada, gameficação, dentre outros, destaque na sua descrição.',
            'required' => true,
            'title' => 'Metodologia',
        ],
        'numero_aulas' => [
            'label' => 'Qual o número de aulas/encontros estimados?',
            'after' => 'De acordo com sua experiência ou ideia, proponha uma quantidade a ser considerada, lembrando, claro, que cada educador poderá adaptar conforme sua necessidade.',
            'required' => true,
            'title' => 'Número de aulas',
        ],
        'resultados' => [
            'label' => 'Quais os resultados?',
            'after' => 'Se a prática já tiver sido implementada, indique quais foram os resultados observados em termos de aprendizagem de algum conteúdo/tópico ou prática, de relações interpessoais entre os estudantes ou de autoconhecimento. Caso a prática seja uma proposição, elenque os possíveis resultados que podem ser obtidos.',
            'required' => true,
            'title' => 'Resultados',
        ],
        'porque_recomenda' => [
            'label' => 'Por que recomenda que sua prática seja reproduzida?',
            'after' => 'Enumere ou descreva os benefícios de ver sua prática implementada por mais educadores comprometidos com a formação em cidadania digital. Fique à vontade para dar sugestões de adaptações e/ou uso em situações específicas.',
            'required' => true,
            'title' => 'Por que recomenda?',
        ],
        'adaptacao' => [
            'label' => 'A sua boa prática é uma adaptação/recriação de outra boa prática? Qual?',
            'after' => 'Se você teve a ideia a partir de alguma outra prática, aproveite para atribuí-la aqui. Referenciar nossas inspirações é uma atitude muito digna e que vai ao encontro da proposta desta plataforma. Se a ideia surgiu de um filme, uma visita a uma exposição, uma conversa informal, também vale a pena citar!',
            'required' => false,
            'title' => 'Adaptação/recriação',
        ],
        'referencias' => [
            'label' => 'Referências',
            'after' => 'Fundamental indicar textos para leitura e aprofundamento que você tenha utilizado, assim como livros, sites e outros materiais que você recomenda. Por favor, informe um item por linha.',
            'required' => false,
            'title' => 'Referências',
        ],
    ];

    private function __construct()
    {
        $postType = new Post('pratica', 'praticas', 'Prática', 'Prática', 'Práticas', '', false, ['post_tag']);
        $postType->setArgs('map_meta_cap', true);
        $postType->setArgs('capability_type', 'pratica');
        $postType->setArgs('capabilities', [
            'read_post' => 'read_pratica',
            'publish_posts' => 'publish_praticas',
            'edit_posts' => 'edit_praticas',
            'edit_others_posts' => 'edit_others_praticas',
            'delete_posts' => 'delete_praticas',
            'delete_others_posts' => 'delete_others_praticas',
            'read_private_posts' => 'read_private_praticas',
            'edit_post' => 'edit_pratica',
            'delete_post' => 'delete_pratica',
        ]);

        $postType->setArgs('supports', ['title', 'comments', 'author']);

        $this->postType = $postType->init();

        $metabox = new Metabox('_detalhes', 'Detalhes da prática', $this->postType);
        $metabox->setFormFunction([$this, 'createForm']);
        $metabox->setSaveFunction([$this, 'saveData']);
        $metabox->init();

        $metaImage = new Metabox('_imagem', 'Imagens', $this->postType);
        $metaImage->setFormFunction([$this, 'addImage']);
        $metaImage->setSaveFunction([$this, 'saveImage']);
        $metaImage->init();

        $metaVideo = new Metabox('_video', 'Vídeos (Youtube/Vimeo)', $this->postType);
        $metaVideo->setFormFunction([$this, 'addVideo']);
        $metaVideo->setSaveFunction([$this, 'saveVideo']);
        $metaVideo->init();

        add_filter('pre_get_posts', [$this, 'includeInFeed']);

        add_filter('the_content', [$this, 'transformContent']);

        $column = new Columns($this->postType);
        $column->appendColumn(['author' => __('Author')]);
        $column->setDataFunction(function ($column) {
            return false;
        });
        $column->init();

        add_action('pre_get_posts', function ($query) {
            if (! is_admin() && $query->is_main_query() && $query->is_tag()) {
                $query->set('post_type', 'pratica');
            }
            return $query;
        });

        $this->pdfCreationProcess();
    }

    private function getData($postId)
    {
        $content = get_post_field('post_content', $postId);
        $data = json_decode($content, true);

        if (json_last_error()) {
            $data = json_decode(strip_tags($content), true);
        }
        return $data;
    }

    public function createForm()
    {
        $post = $GLOBALS['post'];
        $data = $this->getData($post->ID);
        foreach ($this->fields as $name => $details) {
            ?>
            <div class="field">
                <h3><label for="<?php echo $name; ?>"><?php echo $details['label'], $details['required'] ? '<label> <span class="required" title="Campo obrigatório">*</span>' : ''; ?></h3>
                <i><?php echo $details['after']; ?></i><br /><br />
                <?php
                if ($name !== 'coautor') {
                    ?>
                    <textarea class="widefat tiny-mce" rows="10" <?php echo $details['required'] ? 'required="required"' : ''; ?> id="<?php echo $name; ?>" name="_content[<?php echo $name; ?>]"><?php echo isset($data[$name]) ? $data[$name] : ''; ?></textarea>
                    <?php
                } else {
                    ?>
                    <input type="text" class="widefat" <?php echo $details['required'] ? 'required="required"' : ''; ?> id="<?php echo $name; ?>" name="_content[<?php echo $name; ?>]" value="<?php echo isset($data[$name]) ? $data[$name] : ''; ?>"/>
                    <?php
                }
                ?>
            </div>
            <br />
            <?php
        }
        ?>
        <script type="text/javascript">
        jQuery(document).ready(function($){
            $('#publish, #save-post').click(function() {
                 $('h3').removeClass('field-required');
                 $('[required="required"]').each(function(){
                    if ($(this).val().replace(/(<([^>]+)>)/ig,"").trim() === '') {
                        alert('Atenção!\nPor favor, preencha o campo que será indicado.');
                        $('html, body').animate({
                            scrollTop: $(this).parent().offset().top * 0.9
                        }, 500);
                        $(this).siblings('h3').addClass('field-required');
                        return false;
                    }
                 });
            });
        });
        </script>
        <?php
    }

    public function saveData()
    {
        if (! is_user_logged_in()) {
            return false;
        }
        if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX)) {
            return false;
        }
        if (isset($_POST['_content'])) {
            $content = stripslashes_deep($_POST['_content']);
            array_walk($content, function ($val, $key) use (&$content) {
                $content[$key] = trim(strip_tags($val, '<p><strong><em><span><ul><li><a>'));
            });

            $excerpt = $content['contexto'];

            $content = json_encode($content, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

            $post = $GLOBALS['post'] ?? '';
            $wpdb = $GLOBALS['wpdb'] ?? '';
            if (! is_object($post) || ! is_object($wpdb)) {
                return false;
            }

            $wpdb->query($wpdb->prepare('UPDATE wp_posts SET post_content = "%s", post_excerpt = "%s" WHERE ID = %d;', $content, $excerpt, $post->ID));
        }
    }

    public function addImage()
    {
        ?>
        <i>
            Você registrou a prática em imagens? Se sim, insira o arquivo da imagem diretamente de seu dispositivo clicando no botão "Browse". Lembre-se de ter pedido autorização de uso dessa imagem para finalidade educativa (se aparecerem crianças ou adolescentes, autorização de seus pais/responsáveis).
        </i>
        <br />
        <br />
        <div id="_images">
            <?php
            $post = $GLOBALS['post'];
            $images = json_decode(get_post_meta($post->ID, '_images', true), true);
            if (is_array($images)) {
                foreach ($images as $d) {
                    ?>
                    <div class="image-area">
                        <img data-json="<?php echo htmlspecialchars(json_encode($d), ENT_QUOTES, 'UTF-8'); ?>" src="<?php echo $d['data']['thumb']['url']; ?>" />
                        <a href="#remove-image" class="remove-image">&#215;</a>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <input type="hidden" name="_images"/>
        <br />
        <input type="file" id="_new_image" class="widefat" name="_new_image" accept="image/*"/>
        <script type="text/javascript">
        jQuery(document).ready(function($){
            $('form#post').attr('enctype', 'multipart/form-data');
            $('[name="add_image"]').on('click', function(e) {
                e.preventDefault();
                let me = $(this),
                    fd = new FormData();;
                if ($('#_new_image').get(0).files.length === 0) {
                    alert('Atenção! Por favor, selecione a imagem.');
                    return false;
                }
                $(me).hide();
                $('img.process').show();

                let newImage = $('#_new_image');
                newImage = newImage[0].files[0];
                fd.append('_new_image', newImage);

                $.ajax({
                    type: 'post',
                    url: ajaxurl + '?action=imgbb_upload',
                    data: fd,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(d) {
                        if (d.status == 200) {
                            $('#_images').append('<div class="image-area"><img src="' + d.data.thumb.url + '" /><a href="#remove-image" class="remove-image">&#215;</a></div>');
                            $('#_images img:last').data('json', d);
                            $('#_new_image').val('');
                        } else {
                            alert('Atenção! Imagem não encontrada.');
                        }
                        $('img.process').hide();
                        $(me).show();
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr);
                        let errMsg = '';
                        if (xhr.responseText === '') {
                            errMsg = 'Erro ao processar solicitação.\n\nPor favor, tente novamente mais tarde.\n\nDetalhes:\n' + error;
                        } else {
                            errMsg = 'Atenção!\n\n' + xhr.responseText;
                        }
                        alert(errMsg);
                    },
                    complete: function() {
                    }
                });
            });
            $('form').submit(function(){
                let images = []
                $('#_images img').each(function(){
                    images.push($(this).data('json'));
                });
                $('[name="_images"]').val(JSON.stringify(images));
            });
        });
        </script>
        <?php
        submit_button('Adicionar', 'primary', 'add_image');
        ?>
        <img src="<?php echo admin_url('images/loading.gif'); ?>" alt="Processando..." title="Processando..." style="display: none;" class="process" />
        <?php
    }

    public function saveImage()
    {
        if (! is_user_logged_in()) {
            return false;
        }
        if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX)) {
            return false;
        }
        $post = $GLOBALS['post'] ?? '';
        if (! is_object($post)) {
            return false;
        }
        if (isset($_POST['_images'])) {
            update_post_meta($post->ID, '_images', $_POST['_images']);
        }
    }

    public function addVideo()
    {
        ?>
        <i>
            Você registrou a prática em vídeo? Se sim, indique o link do vídeo na respectiva plataforma (por exemplo, Youtube ou Vimeo). Lembre-se de ter pedido autorização de uso de imagem para quem aparece (se crianças, de seus pais).
        </i>
        <br />
        <br />
        <div id="_videos">
            <?php
            $post = $GLOBALS['post'];
            $videos = json_decode(get_post_meta($post->ID, '_videos', true), true);
            if (is_array($videos)) {
                foreach ($videos as $video) {
                    ?>
                    <div class="image-area">
                        <img src="<?php echo $video['src']; ?>" data-site="<?php echo $video['site']; ?>" data-id="<?php echo $video['id']; ?>"/>
                        <a href="#remove-image" class="remove-image">&#215;</a>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <input type="hidden" name="_videos"/>
        <br />
        <input type="url" id="new_video" class="widefat" placeholder="Informe o link do vídeo no Youtube ou Vimeo."/>
        <script type="text/javascript">
        jQuery(document).ready(function($){
            $('[name="add_video"]').on('click', function(e) {
                e.preventDefault();
                let me = $(this);
                    src = $('#new_video').val(),
                    id = '',
                    site = '',
                    regexs = {
                        'vimeo': 'https:\\/\\/(www\\.)?vimeo.com\\/(\\d+)($|\\.*|\\/)',
                        'youtube': 'https:\\/\\/(www\\.)?youtube.com\\/watch\\?v=([0-9A-z]*)($|\\.*|\\/)',
                    },
                    addVideo = function(id, site, thumb) {
                        if (id === '') {
                            alert('Atenção! O link informado é inválido.');
                            return false;
                        }
                        $('#_videos').append('<div class="image-area"><img src="' + thumb + '" data-site="' + site + '"data-id="' + id + '"/><a href="#remove-image" class="remove-image">&#215;</a></div>')
                    };
                if (src.trim() === '') {
                    alert('Por favor, informe a URL do vídeo!');
                    return false;
                }
                $('#new_video').val(''),
                $.each(regexs, function(i, re) {
                    if (match = src.match(new RegExp(re))) {
                        id = match[2]
                        site = i;
                        if (site === 'youtube') {
                            addVideo(id, site, 'https://i1.ytimg.com/vi/' + id + '/mqdefault.jpg');
                        }
                        if (site === 'vimeo') {
                            $.ajax({
                                type: 'get',
                                url: 'https://www.vimeo.com/api/v2/video/' + id + '.json',
                                async: false,
                                dataType: 'jsonp',
                                success: function(d) {
                                    addVideo(id, site, d[0].thumbnail_medium);
                                }
                            });
                        }
                    }
                });
            });
            $('form').submit(function(){
                let videos = []
                $('#_videos img').each(function(){
                    videos.push({
                        'src': $(this).attr('src'),
                        'id': $(this).data('id'),
                        'site': $(this).data('site'),
                    });
                });
                $('[name="_videos"]').val(JSON.stringify(videos));
            });
        });
        </script>
        <?php
        submit_button('Adicionar', 'primary', 'add_video');
        ?>
        <img src="<?php echo admin_url('images/loading.gif'); ?>" alt="Processando..." title="Processando..." style="display: none;" class="process" />
        <?php
    }

    public function saveVideo()
    {
        if (! is_user_logged_in()) {
            return false;
        }
        if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX)) {
            return false;
        }

        $post = $GLOBALS['post'] ?? '';
        if (! is_object($post)) {
            return false;
        }
        if (isset($_POST['_videos'])) {
            update_post_meta($post->ID, '_videos', $_POST['_videos']);
        }
    }

    public function includeInFeed($query)
    {
        if ($query->is_feed() && $query->is_main_query()) {
            $query->set('post_type', [$this->postType]);
        }
        return $query;
    }

    public function transformContent($content)
    {
        $post = $GLOBALS['post'] ?? '';
        if (! isset($post->post_type) || $post->post_type !== 'pratica') {
            return $content;
        }

        $data = $this->getData($post->ID);
        if (empty($data)) {
            return false;
        }
        unset($data['coautor']);
        $html = '';
        foreach ($data as $key => $value) {
            $value = trim($value);
            if (empty($value)) {
                continue;
            }
            $html .= sprintf('<article class="%s">', $key);
            $html .= sprintf('<h3>%s</h3>', $this->fields[$key]['title']);
            $html .= apply_filters('the_excerpt', $value);
            $html .= '</article>';
        }
        $images = json_decode(get_post_meta($post->ID, '_images', true), true);
        if (is_array($images) && ! empty($images)) {
            $html .= '<article class="images-pratica"><h3>Imagens</h3>';
            foreach ($images as $d) {
                $html .= '<a href="' . $d['data']['url'] . '" target="_blank" rel="nofollow" title="Clique para visualizar"><div class="image" style="background: url(\'' . $d['data']['url'] . '\')"></div></a>';
            }
            $html .= '</article>';
        }
        $videos = json_decode(get_post_meta($post->ID, '_videos', true), true);
        if (is_array($videos) && ! empty($videos)) {
            $html .= '<article class="videos-pratica"><h3>Vídeos</h3>';
            foreach ($videos as $video) {
                $videoUrl = $video['site'] === 'youtube' ? sprintf('https://youtube.com/watch?v=%s', $video['id']) : sprintf('https://vimeo.com/%s', $video['id']);
                $html .= '<a href="' . $videoUrl . '" target="_blank" rel="nofollow" title="Clique para visualizar"><div class="video" style="background: url(\'' . $video['src'] . '\')"></div></a>';
            }
            $html .= '</article>';
        }
        return $html;
    }

    private function pdfCreationProcess()
    {
        add_action('admin_head', function () {
            $post = $GLOBALS['post'] ?? '';
            if (! is_object($post)) {
                return false;
            }
            if ($post->post_type !== $this->postType || $post->post_status !== 'publish') {
                return false;
            }

            $pdf = get_post_meta($post->ID, '_pdf', true);
            if (empty($pdf)) {
                return false;
            }
            delete_post_meta($post->ID, '_pdf');

            ob_start();
            require(plugin_dir_path(dirname(__DIR__)) . 'view/pdf/pratica.php');
            $html = ob_get_clean();

            $pdf = new Pdf();
            $pdf->create($post, $html);
        });

        add_action('save_post', function () {
            if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX)) {
                return false;
            }

            $post = $GLOBALS['post'] ?? '';
            if (! is_object($post)) {
                return false;
            }

            if ($post->post_type !== $this->postType || $post->post_status !== 'publish') {
                return false;
            }
            update_post_meta($post->ID, '_pdf', 1);
        });
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
