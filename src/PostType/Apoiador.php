<?php
namespace PilaresDoFuturo\PostType;

use AndreKeher\WPDP\Metabox;
use AndreKeher\WPDP\Post;
use PilaresDoFuturo\Meta\Resource;

class Apoiador
{
    private static $instance;
    private $postType;

    private function __construct()
    {
        $postType = new Post('apoiador', 'apoiadores', 'Apoiador', 'Apoiador', 'Apoiadores', '', false);
        $postType->setArgs('supports', ['title', 'thumbnail']);

        $labels = $postType->getArgs('labels');
        $labels['featured_image'] = 'Logo do apoiador';
        $labels['set_featured_image'] = 'Configurar logo do apoiador';
        $labels['remove_featured_image'] = 'Remover logo do apoiador';
        $labels['use_featured_image'] = 'Usar como logo do apoiador';
        $labels['insert_into_item'] = 'Inserir logo';
        $postType->setArgs('labels', $labels);
        $postType->setArgs('has_archive', false);
        $postType->setArgs('publicly_queryable', false);
        $postType->setArgs('exclude_from_search', true);

        $this->postType = $postType->init();

        $link = new Resource($this->postType, '_url', 'Site do apoiador', false);
        $link->init();

        add_filter('post_type_link', [$this, 'updateLink'], 1, 3);
    }

    public function updateLink($link, $post = 0)
    {
        if ($post->post_type === $this->postType) {
            return get_post_meta($post->ID, '_url', true);
        }
        return $link;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
