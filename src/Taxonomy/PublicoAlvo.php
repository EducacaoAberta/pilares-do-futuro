<?php
namespace PilaresDoFuturo\Taxonomy;

use AndreKeher\WPDP\Taxonomy;

class PublicoAlvo
{
    private static $instance;

    private function __construct()
    {
        $tax = new Taxonomy('publico_alvo', 'praticas/publicos-alvos', 'Público Alvo', ['pratica'], 'Público Alvo', 'Público Alvo', true);
        $tax->setArg('capabilities', [
            'assign_terms' => 'edit_praticas'
        ]);
        $tax->init();
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
