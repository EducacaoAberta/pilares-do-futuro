<?php
namespace PilaresDoFuturo\Taxonomy;

use AndreKeher\WPDP\Taxonomy;

class Tipo
{
    private static $instance;
    private static $postType;

    private function __construct()
    {
        $tax = new Taxonomy('tipo', 'materiais/tipos', 'Tipo', self::$postType, 'Tipo', 'Tipos', true);
        $tax->init();
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance($postType)
    {
        self::$postType = $postType;
        
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
