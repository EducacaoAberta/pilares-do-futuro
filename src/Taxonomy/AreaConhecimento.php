<?php
namespace PilaresDoFuturo\Taxonomy;

use AndreKeher\WPDP\Taxonomy;

class AreaConhecimento
{
    private static $instance;

    private function __construct()
    {
        $tax = new Taxonomy('area_do_conhecimento', 'praticas/areas-do-conhecimento', 'Área do conhecimento', ['pratica'], 'Área do conhecimento', 'Áreas do conhecimento', true);
        $tax->setArg('capabilities', [
            'assign_terms' => 'edit_praticas'
        ]);
        $tax->init();
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
