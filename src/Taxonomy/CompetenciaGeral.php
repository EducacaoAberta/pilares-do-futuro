<?php
namespace PilaresDoFuturo\Taxonomy;

use AndreKeher\WPDP\Taxonomy;

class CompetenciaGeral
{
    private static $instance;

    private function __construct()
    {
        $tax = new Taxonomy('competencia_geral', 'praticas/competencias-gerais-da-bncc', 'Competência geral da BNCC', ['pratica'], 'Competência geral da BNCC', 'Competências gerais da BNCC', true);
        $tax->setArg('capabilities', [
            'assign_terms' => 'edit_praticas'
        ]);
        $tax->init();
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
