<?php
namespace PilaresDoFuturo\Taxonomy;

use AndreKeher\WPDP\Taxonomy;

class Tema
{
    private static $instance;

    private function __construct()
    {
        $tax = new Taxonomy('tema', 'praticas/temas', 'Tema', ['pratica'], 'Tema', 'Temas', true);
        $tax->setArg('capabilities', [
            'assign_terms' => 'edit_praticas'
        ]);
        $tax->init();
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
