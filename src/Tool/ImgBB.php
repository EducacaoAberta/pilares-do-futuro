<?php
namespace PilaresDoFuturo\Tool;

use AndreKeher\WPDP\Ajax;

class ImgBB
{
    private static $instance;
    
    private function __construct()
    {
        $ajax = new Ajax('imgbb_upload');
        $ajax->setFunction([$this, 'doUpload']);
        $ajax->init();
    }
    
    public function doUpload()
    {
        if (! is_user_logged_in()) {
            return false;
        }
        
        if (isset($_FILES['_new_image']) && empty($_FILES['_new_image']['error'])) {
            $img = $_FILES['_new_image'];
            
            $fileOpen = finfo_open(FILEINFO_MIME_TYPE);
            $fileInfo = finfo_file($fileOpen, $img['tmp_name']);
            if (strpos($fileInfo, 'image/') === false) {
                return false;
            }
            
            if (! defined('IMGBB_API_KEY')) {
                return false;
            }
            
            $ch = curl_init('https://api.imgbb.com/1/upload?key=' . IMGBB_API_KEY);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, [
                'image' => base64_encode(file_get_contents($img['tmp_name']))
            ]);
            
            $response = curl_exec($ch);
            
            $response = json_decode($response, true);
            
            if (isset($response['status']) && $response['status'] == 200) {
                header('Content-type: application/json');
                echo json_encode($response);
            }
            
            curl_close($ch);
            die;
        }
    }
    
    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
