<?php

namespace PilaresDoFuturo\Tool;

use AndreKeher\WPDP\Ajax;

class Pdf
{
    public function create($post, $html)
    {
        if (! is_object($post)) {
            return false;
        }
        if ($post->post_status !== 'publish') {
            return false;
        }

        try {
            $css1 = file_get_contents(get_template_directory_uri() . '/style.css');
            $css2 = file_get_contents(plugin_dir_path(dirname(__DIR__)) . 'css/pdf.css');

            $mpdf = new \Mpdf\Mpdf();
            $mpdf->useSubstitutions = true;
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->WriteHTML($css1 . $css2, 1);
            $mpdf->WriteHTML($html, 2);

            $filePath = WP_CONTENT_DIR . sprintf('/uploads/pdf/%s-%d-%s.pdf', $post->post_type, $post->ID, $post->post_name);
            if (file_exists($filePath . '.pdf')) {
                unlink($filePath . '.pdf');
            }

            $mpdf->Output($filePath, \Mpdf\Output\Destination::FILE);
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
    }
}
