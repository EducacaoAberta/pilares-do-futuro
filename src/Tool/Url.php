<?php

namespace PilaresDoFuturo\Tool;

use AndreKeher\WPDP\Ajax;

class Url
{
    private static $instance;

    private function __construct()
    {
        $ajaxCall = new Ajax('get_headers');
        $ajaxCall->setFunction([$this, 'getHeaders']);
        $ajaxCall->init();
    }

    public static function getHeaders($url = '')
    {
        $data = [];
        if (empty($url) && isset($_GET['url'])) {
            $url = $_GET['url'];
        }
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            $data = get_headers($url);
        }
        header('Content-type: application/json; charset=UTF-8;');
        echo json_encode($data);
        die;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
