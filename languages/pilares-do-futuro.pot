# Copyright (C) 2020 André Keher
# This file is distributed under the same license as the Pilares do Futuro plugin.
msgid ""
msgstr ""
"Project-Id-Version: Pilares do Futuro 1.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/pilares-do-futuro\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2020-03-03T04:18:56+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.4.0\n"
"X-Domain: pilares-do-futuro\n"

#. Plugin Name of the plugin
msgid "Pilares do Futuro"
msgstr ""

#. Author of the plugin
msgid "André Keher"
msgstr ""

#. Author URI of the plugin
msgid "https://github.com/andrekeher/"
msgstr ""

#: index.php:42
msgid "Curator"
msgstr ""

#: index.php:44
msgid "Practice author"
msgstr ""

#: src/Custom/User.php:43
msgid "International"
msgstr ""

#: src/Custom/User.php:87
#: src/Custom/User.php:126
msgid "State"
msgstr ""

#: src/Custom/User.php:92
msgid "Enter the characters in the image"
msgstr ""

#: src/Custom/User.php:105
msgid "<strong>ERROR</strong>: Enter your state."
msgstr ""

#: src/Custom/User.php:108
msgid "<strong>ERROR</strong>: Enter the characters in the image."
msgstr ""
