<?php
global $post;
setup_postdata($post);
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <div class="bg-aviao">
            <div class="center">
                <div class="entry-meta">
                    Boa prática
                </div>
                <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
            </div>
        </div>
    </header>
    <div class="autor-area">
        <p class="autor">Criado por: <a href="<?php echo get_author_posts_url(get_the_author_meta('ID'), get_the_author_meta('user_nicename')); ?>"><?php the_author(); ?></a>
        <?php
        $data = strip_tags($post->post_content);
        $coautor = json_decode($data, true)['coautor'];
        if (!empty($coautor)) {
            ?>
            <p class="autor">Coautoria:
            <?php
            printf('<a href="%s">%s</a>', site_url('?s=' . $coautor), $coautor);
            ?>
            </p>
            <?php
        }
        ?>
    </div>
    <div class="center">
        <div class="entry-content">
            <div class="right">
                <?php the_content(); ?>
                <div class="pratica-rodape">
                    As imagens e vídeos indicados nesta prática não estão sob licença CC BY NC, caso queira reutilizá-los, entre em contato com o autor da prática pelo comentário.
                </div>
            </div>
            <div class="left">
                <section>
                    <?php
                    $taxs = get_object_taxonomies($post);
                    unset($taxs[array_search('post_tag', $taxs)]);
                    foreach ($taxs as $tax) {
                        $htmlList = get_the_term_list($post, $tax, '<ul><li>', '</li><li>', '</li></ul>');
                        if (!empty($htmlList)) {
                            printf('<h4>%s</h4>', get_taxonomy_labels(get_taxonomy($tax))->all_items);
                            echo $htmlList;
                        }
                    }
                    ?>
                </section>
                <section class="tags">
                    <?php
                    $tags = get_the_tag_list('<ul><li>', '</li><li>', '</li></ul>');
                    if (! empty($tags)) {
                        echo '<h4>Tags:</h4>', $tags;
                    }
                    ?>
                </section>
            </div>
        </div>
    </div>
</article>
<?php
wp_reset_postdata();
